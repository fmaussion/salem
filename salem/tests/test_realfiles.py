"""Tests on real world files. These files are large and contained in
an online directory:

Make the files available in order to run these tests.

Be sure ncl is installed on your sysem
"""
from __future__ import division

import unittest
import os
import shutil
import subprocess
import time

import netCDF4
import numpy as np
from numpy.testing import assert_allclose

from salem import Grid
from salem import datasets
from salem import wrf

myenv = os.environ.copy()

test_dir = '/home/mowglie/disk/TMP/Salem_Testfiles'

tmp_dir = os.path.join(os.path.dirname(__file__), 'tmp')

def clean_dir():
    if os.path.exists(tmp_dir):
        shutil.rmtree(tmp_dir)
    os.makedirs(tmp_dir)

def run_ncl_script(file_path, file_out):

    scriptf = os.path.join(tmp_dir, 'run.sh')
    if os.path.exists(scriptf):
        os.remove(scriptf)

    with open(scriptf, 'w') as f:
        f.write('#!/bin/bash\n')
        f.write('\n')
        f.write('export NCARG_ROOT=/home/mowglie/.bin/ncl-6.3.0\n')
        f.write('export PATH=$NCARG_ROOT/bin:$PATH\n')
        f.write('ncl ' + file_path + '\n')
        f.write('exit 0\n')

    cmd = '. ' + scriptf
    _ = subprocess.check_output(cmd, shell=True, executable='/bin/bash')


class TestDataset(unittest.TestCase):

    def setUp(self):
        clean_dir()

    def tearDown(self):
        pass
        # if os.path.exists(tmp_dir):
        #

    def test_unstagger(self):

        wf = os.path.join(test_dir, 'wrfout_d01_2008-10-26.nc')
        nc = netCDF4.Dataset(wf)

        ref = nc['PH'][:]
        ref = 0.5 * (ref[:, :-1, ...] + ref[:, 1:, ...])

        # Own constructor
        v = wrf.Unstaggerer(nc['PH'])
        assert_allclose(v[:], ref)
        assert_allclose(v[0:3, 2:12, ...],
                        ref[0:3, 2:12, ...])
        assert_allclose(v[:, 2:12, ...],
                        ref[:, 2:12, ...])
        assert_allclose(v[0:3, 2:12, 50:60, 45:67],
                        ref[0:3, 2:12, 50:60, 45:67])
        assert_allclose(v[1:3, 2:, 50:60, 45:67],
                        ref[1:3, 2:, 50:60, 45:67])
        assert_allclose(v[1:3, :-2, 50:60, 45:67],
                        ref[1:3, :-2, 50:60, 45:67])
        assert_allclose(v[1:3, 2:-4, 50:60, 45:67],
                        ref[1:3, 2:-4, 50:60, 45:67])
        assert_allclose(v[[0, 4, 5], ...],
                        ref[[0, 4, 5], ...])
        assert_allclose(v[..., [0, 4, 5]],
                        ref[..., [0, 4, 5]])
        # TODO: this is an issue
        assert_allclose(v[0, ...], ref[0:0, ...])

        # Under WRF
        nc = datasets.WRF(wf)
        assert_allclose(nc.get_vardata('PH'), ref)
        nc.set_subset(corners=((10, 12), (60, 42)), crs=nc.grid)
        assert_allclose(nc.get_vardata('PH'), ref[..., 12:43, 10:61])
        nc.set_period(1, 3)
        assert_allclose(nc.get_vardata('PH'), ref[1:4, ..., 12:43, 10:61])


    def test_ncl_diagvars(self):

        wf = os.path.join(test_dir, 'wrfout_d01_2008-10-26.nc')
        ncl_script = os.path.join(tmp_dir, 'nclscrip.ncl')
        ncl_out = os.path.join(tmp_dir, 'nclout.nc')

        w = datasets.WRF(wf)

        with open(ncl_script, 'w') as f:
            f.write('load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"\n')
            f.write('load "$NCARG_ROOT/lib/ncarg/nclscripts/wrf/WRFUserARW.ncl"\n')
            f.write('a = addfile("{}","r")\n'.format(wf))
            f.write('tk = wrf_user_getvar(a,"tk",-1)\n')
            f.write('slp = wrf_user_getvar(a,"slp",-1)\n')
            f.write('ncdf = addfile("{}" ,"c")\n'.format(ncl_out))
            f.write('filedimdef(ncdf,"time",-1,True)\n')
            f.write('ncdf->TK = tk\n')
            f.write('ncdf->SLP = slp\n')
            f.write('ncdf->SLP = slp\n')
            f.write('exit()\n')

        run_ncl_script(ncl_script, ncl_out)
        self.assertTrue(os.path.exists(ncl_out))

        nc = netCDF4.Dataset(ncl_out)
        ref = nc.variables['TK'][:]
        tot = w.get_vardata('TK')
        assert_allclose(ref, tot, rtol=1e-6)

        ref = nc.variables['SLP'][:]
        tot = w.get_vardata('SLP')
        assert_allclose(ref, tot, rtol=1e-6)

    def test_staggeredcoords(self):

        wf = os.path.join(test_dir, 'wrfout_d01_2008-10-26.nc')
        nc = datasets.GeoNetcdf(wf)
        lon, lat = nc.grid.xstagg_ll_coordinates
        assert_allclose(np.squeeze(nc.variables['XLONG_U'][0, ...]), lon,
                        atol=1e-4)
        assert_allclose(np.squeeze(nc.variables['XLAT_U'][0, ...]), lat,
                        atol=1e-4)
        lon, lat = nc.grid.ystagg_ll_coordinates
        assert_allclose(np.squeeze(nc.variables['XLONG_V'][0, ...]), lon,
                        atol=1e-4)
        assert_allclose(np.squeeze(nc.variables['XLAT_V'][0, ...]), lat,
                        atol=1e-4)

    def test_har(self):

        # HAR
        d = datasets.GeoNetcdf(os.path.join(test_dir,
                                            'har_d30km_y_2d_t2_2000.nc'))
        reflon = np.squeeze(d.get_vardata('lon'))
        reflat = np.squeeze(d.get_vardata('lat'))
        mylon, mylat = d.grid.ll_coordinates
        np.testing.assert_allclose(reflon, mylon, atol=1e-5)
        np.testing.assert_allclose(reflat, mylat, atol=1e-5)


def plot_array(array):
    """Quick plot of a 2D array.

    Parameters
    ----------
    array: array to plot
    """
    import matplotlib.pyplot as plt
    fig = plt.figure(facecolor='white')
    ax = plt.imshow(array, interpolation='none')
    cbar = fig.colorbar(ax, orientation = 'horizontal')
    plt.tight_layout()
    plt.show()







