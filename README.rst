.. -*- rst -*- -*- restructuredtext -*-
.. This file should be written using restructured text conventions

=====
Salem
=====


.. image:: https://coveralls.io/repos/fmaussion/salem/badge.svg?branch=master&service=bitbucket
  :target: https://coveralls.io/bitbucket/fmaussion/salem?branch=master

Salem is a `cat <https://drive.google.com/file/d/0B-0AsTwFw61uSE0zaktOOVN5X1E/view?usp
=sharing>`_, but Salem is also a small library to do some geoscientific data
handling. Together with `cleo <https://bitbucket.org/fmaussion/cleo>`_, they
provide a framework to work, analyse, and plot climate and geoscientific data.

Better, bigger projects are found out there (see links below). Salem basically
reinvents the wheel, but in the way I want the wheel to be. Salem is quite
young but well tested, and might be useful to a few: if you are using the
`WRF <http://www.wrf-model.org>`_ model for example, or if you don't want to
bother about some aspects of georeferencing of your netCDF files. Another
reason might be that you are a user of the IDL WAVE library and want to try
Python, too.

Installation
------------

Salem needs the libraries numpy, scipy, pyproj and netCDF4. To get the full
functionalities you will also need the xray, rasterio, pandas, geopandas and
shapely libraries.

After installing those, a simple *pip install* should suffice::

    $ pip install git+https://bitbucket.org/fmaussion/salem.git#egg=Salem

This will build the latest repository version. Since Salem is constantly
evolving, I don't expect to make a release plan and stuff.


Classes
-------

**Grid**
    Handling of structured (gridded) map projections

**GeoDataset**
    Handling of gridded data files (subsetting, regions of interest,
    timeseries)

**GeoDataset/GeoTiff**
    Handling of georeferenced TIFF files

**GeoDataset/GeoNetcdf**
    Handling of gridded netCDF files (they should be approx. CF compliant or
    should be implemented in Salem, such as WRF)

**GeoDataset/GeoNetcdf/WRF**
    Handling of WRF output files (including diagnostic variables such as
    SLP, TK, etc.)


Getting started with Salem
--------------------------

Coming soon...


Projects that are bigger/better/stronger than Salem
---------------------------------------------------

Non exhaustive list:

Iris
    Very very impressive, but not python3 and quite a pain to install
    without Anaconda
    http://scitools.org.uk/iris/index.html
xray
    Pandas in ND.
    https://github.com/xray/xray
pyClim
    Not very documented or advertised but by looking at the code it seems cool.
    http://servforge.legi.grenoble-inp.fr/projects/soft-pyclim

Salem will proprably use xray in the close future.


About
-----

:License:
    GNU GPLv3

:Author:
    Fabien Maussion - fabien.maussion@uibk.ac.at
